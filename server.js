'use strict'

const express = require('express')
const app = express()
const port = 8000
app.get('/movie', (req, res) => {
  const result = {
    result: [{
      title: 'fake title 1',
      image_url: 'fake image url',
      overview: 'fake overview'
    }, {
      title: 'fake title 1',
      image_url: 'fake image url',
      overview: 'fake overview'
    }]
  }
  res.json(result)
})

app.listen(port, () => {
  console.log(`started at ${port}`);

})